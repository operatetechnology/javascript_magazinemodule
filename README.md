# Installation

1. Add magazinemodule_magazine.js to scripts
2. Create new Magazine prototype
3. Initialize magazine with initialize() method

## Magazine prototype
	MagazineModule.Magazine(url, magazineId, parent, callback);
	
*Magazine prototype arguments*

* url: http://example.org/path/
* magazineId: 106566
* parent: reference to DOM element
* callback: function to call when page in iframe is turned

*Magazine callback arguments*

* message: text message from magazine
* currentPage: current page after page turn
* totalPages: total pages in magazine

## Magazine callback
	var magazineCallback = function(message, currentPage, totalPages) {}

# Example
See example/example.html


