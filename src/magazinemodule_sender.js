var MagazineModule = MagazineModule || {};

MagazineModule.senderRegistry = [];

MagazineModule.Sender = function () {
	
	this.targetType = '';
	this.target = '';

};

MagazineModule.Sender.prototype.notify = function(currentPage, totalPages) {
	
	if (this.targetType === 'POSTMESSAGE') {
		this.target.postMessage({
			message: 'PAGETURN',
			currentPage: currentPage,
			totalPages: totalPages
		}, '*');
	}

	if (this.targetType === 'MESSAGECHANNEL') {
		this.target.postMessage({
			message: 'PAGETURN',
			currentPage: currentPage,
			totalPages: totalPages
		});
	}
	
};

MagazineModule.Sender.prototype.initialize = function() {
	
	if (window.MessageChannel) {
		this.attachEventlistenerMessageChannel();
	} else {
		this.attachEventlistenerPostMessage();
	}
};

MagazineModule.Sender.prototype.attachEventlistenerMessageChannel = function() {
	
	var _this = this;
	onmessage = function(event) {
		if (event.data.message === 'PORT') {
			_this.target = event.ports[0];
			_this.targetType = 'MESSAGECHANNEL';
			event.ports[0].postMessage({
				message: 'OK'
			});
		}
	};

};

MagazineModule.Sender.prototype.attachEventlistenerPostMessage = function() {
	
	var _this = this;
	if (window.addEventListener) {
		window.addEventListener('message', function(event) {
			if (event.data.message === 'HELLO') {
				_this.target = event.source;
				_this.targetType = 'POSTMESSAGE';
				event.source.postMessage({
					message: 'OK'
				}, '*');
			}
			
		}, false);
	}

};