var MagazineModule = MagazineModule || {};

MagazineModule.Magazine = function (url, mid, container, callback) {

	this.container = container;
	this.src = url + mid;
	this.width = null;
	this.height = null;
	this.iframe = document.createElement('iframe');
	
	if (typeof callback === "function") {
		this.callback = callback;
	}

	// Get dimensions of container
	if (container === undefined || container === null) {
		console.log("Parent container is null or undefined. Sure the document is ready?");
	} else  {
		this.height = container.offsetHeight;
		this.width = container.offsetWidth;	
	}
	
	// Check and set defaults 
	if (this.height === 'undefined' || this.height <= 0) { this.height = 800; }
	if (this.width === 'undefined' || this.width <= 0) { this.width = 800; }

	this.width = this.width + 'px';
	this.height = this.height + 'px';

};

MagazineModule.Magazine.prototype.initialize = function() {
	this.iframe.src = this.src;
	this.iframe.style.height = this.height;
	this.iframe.style.width = this.width;

	if (this.container) {
		this.container.appendChild(this.iframe);
	}
	
	this._attachEventlistenerAndConnect();
	
};

MagazineModule.Magazine.prototype._attachEventlistenerAndConnect = function() {
	
	if (window.MessageChannel) {
		this._attachEventlistenerAndConnectMessageChannel();
	} else {
		this._attachEventlistenerPostMessage();
	}
	
};

MagazineModule.Magazine.prototype._attachEventlistenerAndConnectMessageChannel = function() {
	
	var _this = this;
	var messageChannel = new MessageChannel();
	
	messageChannel.port1.onmessage = function(event) {

		if (event.data.message === 'PAGETURN') {
			_this.callback(event.data.message, event.data.currentPage, event.data.totalPages);
		}
        
	};

	if (window.addEventListener) {
		this.iframe.addEventListener('load', function() {
			_this.iframe.contentWindow.postMessage({
				message: 'PORT'
			}, '*', [messageChannel.port2]);
		}, false);
	}
	

};

MagazineModule.Magazine.prototype._attachEventlistenerAndConnectPostMessage = function() {
	
	var _this = this;

	if (window.addEventListener) {
		window.addEventListener('message',function(event) {
			
			if (event.data.message === 'PAGETURN') {
				_this.callback(event.data.message, event.data.currentPage, event.data.totalPages);
			}

		}, false);
	}
	
	if (window.addEventListener) {
		this.iframe.addEventListener('load', function() {
			_this.iframe.contentWindow.postMessage({
				message: 'HELLO'
			}, '*');
		});
	}

};